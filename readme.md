<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Dynamic Restaurant App 
The application allows you to do simple restaurant food order/reserve management system. User can order their desire foods from the list autonomously. If the admin will approve their food request, they will get an email notification. Admin is able to change food list, category, sliders etch dynamically from admin dashboard.   

## Screenshots

![ScreenShot](/screenshot/screencapture1.png)
![ScreenShot](/screenshot/screencapture2.png)
![ScreenShot](/screenshot/screencapture3.png)
![ScreenShot](/screenshot/screencapture4.png)
![ScreenShot](/screenshot/screencapture5.png)
![ScreenShot](/screenshot/screencapture6.png)
![ScreenShot](/screenshot/screencapture7.png)
![ScreenShot](/screenshot/screencapture8.png)
![ScreenShot](/screenshot/screencapture9.png)

## Security Vulnerabilities

If you discover any error or problems, please send an e-mail to Md. Saimun Hossain via [hossainmdsaimun@gmail.com](mailto:hossainmdsaimun@gmail.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).