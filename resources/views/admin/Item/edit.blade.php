@extends('layouts.app')

@section('title','Item')

@push('css')

@endpush

@section('content')
<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            @include('layouts.partial.msg')
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Add New Item</h4>
                                </div>
                                <div class="card-content">
                                    <form method="POST" action="{{ route('item.update', $item->id) }}" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Category</label>
                                                    <select name="category" class="form-control">
                                                        @foreach($categories as $category)
                                                            <option {{ $category->id == $item->category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Name</label>
                                                    <input type="text" value="{{ $item->name }}" class="form-control" name="name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Description</label>
                                                    <textarea name="description" class="form-control" cols="30" rows="3">{{ $item->description }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Price</label>
                                                    <input type="numer" value="{{ $item->price }}" class="form-control" name="price">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="control-label">Image</label>
                                                <input type="file" name="image">
                                            </div>
                                        </div>
                                        <a href="{{ route('item.index') }}" class="btn btn-success"><i class="material-icons">fast_rewind</i></a>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@push('scripts')

@endpush