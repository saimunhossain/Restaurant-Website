<footer class="footer">
                <div class="container-fluid">
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="https://gitlab.com/saimunhossain">Saimun Hossain</a>, made with love
                    </p>
                </div>
            </footer>